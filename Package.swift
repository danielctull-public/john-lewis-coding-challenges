// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "John Lewis Coding Challenges",
    targets: [

        .target(
            name: "Sources",
            path: "Sources"),

        .testTarget(
            name: "Tests",
            dependencies: ["Sources"],
            path: "Tests"),
    ]
)
