
import XCTest
import Sources

final class Challenge3Tests: XCTestCase {

    func testMoveRight() throws {

        let start = ["#############",
                     "#p        * #",
                     "#     b  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "# p       * #",
                        "#     b  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "R")
        XCTAssertEqual(new, expected)
    }

    func testMoveLeft() throws {

        let start = ["#############",
                     "# p       * #",
                     "#     b  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "#p        * #",
                        "#     b  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "L")
        XCTAssertEqual(new, expected)
    }

    func testMoveDown() throws {

        let start = ["#############",
                     "# p       * #",
                     "#     b  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "#         * #",
                        "# p   b  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "D")
        XCTAssertEqual(new, expected)
    }

    func testMoveUp() throws {

        let start = ["#############",
                     "#         * #",
                     "# p   b  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "# p       * #",
                        "#     b  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "U")
        XCTAssertEqual(new, expected)
    }

    func testBoxRight() throws {

        let start = ["#############",
                     "#         * #",
                     "#    pb  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "#         * #",
                        "#     pb b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "R")
        XCTAssertEqual(new, expected)
    }

    func testBoxLeft() throws {

        let start = ["#############",
                     "#         * #",
                     "#     bp b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "#         * #",
                        "#    bp  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "L")
        XCTAssertEqual(new, expected)
    }

    func testBoxDown() throws {

        let start = ["#############",
                     "#     p   * #",
                     "#     b  b  #",
                     "# *         #",
                     "#############"]

        let expected = ["#############",
                        "#         * #",
                        "#     p  b  #",
                        "# *   b     #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "D")
        XCTAssertEqual(new, expected)
    }

    func testBoxUp() throws {

        let start = ["#############",
                     "#         * #",
                     "#     b  b  #",
                     "# *   p     #",
                     "#############"]

        let expected = ["#############",
                        "#     b   * #",
                        "#     p  b  #",
                        "# *         #",
                        "#############"]

        let new = try processSokobanMove(board: start, move: "U")
        XCTAssertEqual(new, expected)
    }

    func testWallLeft() throws {
        let start = ["#p"]

        XCTAssertThrowsError(try processSokobanMove(board: start, move: "L")) {
            let error = $0 as? MovementError
            XCTAssertEqual(error?.reason, .playerOnWall)
            XCTAssertEqual(error?.position.x, 0)
            XCTAssertEqual(error?.position.y, 0)
        }
    }

    func testWallRight() throws {
        let start = ["p#"]

        XCTAssertThrowsError(try processSokobanMove(board: start, move: "R")) {
            let error = $0 as? MovementError
            XCTAssertEqual(error?.reason, .playerOnWall)
            XCTAssertEqual(error?.position.x, 1)
            XCTAssertEqual(error?.position.y, 0)
        }
    }

    func testWallUp() throws {

        let start = ["#",
                     "p"]

        XCTAssertThrowsError(try processSokobanMove(board: start, move: "U")) {
            let error = $0 as? MovementError
            XCTAssertEqual(error?.reason, .playerOnWall)
            XCTAssertEqual(error?.position.x, 0)
            XCTAssertEqual(error?.position.y, 0)
        }
    }

    func testWallDown() throws {

        let start = ["p",
                     "#"]

        XCTAssertThrowsError(try processSokobanMove(board: start, move: "D")) {
            let error = $0 as? MovementError
            XCTAssertEqual(error?.reason, .playerOnWall)
            XCTAssertEqual(error?.position.x, 0)
            XCTAssertEqual(error?.position.y, 1)
        }
    }
}
