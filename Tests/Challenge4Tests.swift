import XCTest
import Sources

final class Challenge4Tests: XCTestCase {

    func testExample() throws {

        let input = "190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff"
        let expected = "190111:Activated:ffff,190111:Available:cccc,190112:Activated:bbbb,190112:Available:aaaa,190110:Redeemed:dddd,190110:Expired:eeee"

        let vouchers = try sortVouchers(input)
        XCTAssertEqual(vouchers, expected)
    }

    func testLong() throws {

        let input = "190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff,190111:Expired:gggg,190111:Redeemed:hhhh"
        let expected = "190111:Activated:ffff,190111:Available:cccc,190112:Activated:bbbb,190112:Available:aaaa,190111:Redeemed:hhhh,190111:Expired:gggg,190110:Redeemed:dddd,190110:Expired:eeee"

        let vouchers = try sortVouchers(input)
        XCTAssertEqual(vouchers, expected)
    }
}
